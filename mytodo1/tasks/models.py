from django.db import models

class Task(models.Model):
    STATUS_CHOICES = [
        ('в процессе', 'В процессе'),
        ('выполнено', 'Выполнено'),
        ('ожидает', 'Ожидает'),
    ]

    title = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='в процессе')
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
    
     